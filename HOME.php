<?php

    include('Header.php');
    ?>
<div class="section group">
	<div class="col span_1_of_3">
    <div id="Image_shadow">
    <img class="Image_size" src="images/product_6.jpg">
    <div class="Product_info">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
<p class="product_data">PUNJABI SPECIAL </p><br>
<p class="product_data">|40 pieces in carton|</p>
<p class="product_data">  RS:600 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
    </div>
    </div>
	</div>
	<div class="col span_1_of_3">
	<div id="Image_shadow">
<img class="Image_size" src="images/product_1.jpg">
<div class="Product_info">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span><br>
<p class="product_data">CHOC DIP </p><br>
<p class="product_data">|60 pieces in carton|</p>
<p class="product_data"> RS:250 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
</div>
  </div>
	</div>
	<div class="col span_1_of_3">
    <div id="Image_shadow">
  <img class="Image_size" src="images/product_2.jpg">
  <div class="Product_info">
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span><br>
<p class="product_data">Big Slice Kulfa </p><br>
<p class="product_data">|30 pieces in carton|</p>
<p class="product_data">  RS:250 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
  </div>
    </div>
	</div>
</div>
<div class="section group">
	<div class="col span_1_of_3">
    <div id="Image_shadow">
    <img class="Image_size" src="images/product_3.jpg">
    <div class="Product_info">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
<p class="product_data">Nutty Stick </p><br>
<p class="product_data">|30 pieces in carton|</p>
<p class="product_data">  RS: 450 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
    </div>
    </div>

	</div>
	<div class="col span_1_of_3">
	<div id="Image_shadow">
<img class="Image_size" src="images/product_4.jpg">
<div class="Product_info">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span><br>
<p class="product_data">FEAST Stick </p><br>
<p class="product_data">| 20 pieces in carton|</p>
<p class="product_data">  RS:500 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
</div>
  </div>
	</div>
	<div class="col span_1_of_3">
    <div id="Image_shadow">
  <img class="Image_size" src="images/product_5.jpg">
  <div class="Product_info">
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span><br>
<p class="product_data">CHOCO BAR </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data"> |  RS:420 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
  </div>
    </div>
	</div>
</div>
<div class="section group">
	<div class="col span_1_of_3">
    <div id="Image_shadow">
    <img class="Image_size" src="images/product_13.jpg">
    <div class="Product_info">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
<p class="product_data"> DONUT  </p><br>
<p class="product_data">|17 pieces in carton|</p>
<p class="product_data"> RS:400 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
    </div>
    </div>
	</div>
	<div class="col span_1_of_3">
	<div id="Image_shadow">
<img class="Image_size" src="images/product_7.jpg">
<div class="Product_info">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span><br>
<p class="product_data">RED ANAAR Stick </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data">  RS:210 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
</div>
  </div>
	</div>
	<div class="col span_1_of_3">
    <div id="Image_shadow">
  <img class="Image_size" src="images/product_8.jpg">
  <div class="Product_info">
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span><br>
<p class="product_data">Strawbery Stick </p><br>
<p class="product_data">|60 pieces in carton|</p>
<p class="product_data">  RS: 250 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
  </div>
    </div>
	</div>
</div>
<div class="pagination">
  <a href="HOME.php">&laquo;</a>
  <a href="HOME.php" class="active">1</a>
  <a href="page2.php" >2</a>
  <a href="page3.php">3</a>

  <a href="page2.php">&raquo;</a>
</div>
<?php
include('footer.php')
 ?>
