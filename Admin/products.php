<?php
include('Header.php');
if (empty($_SESSION['id'])) {
  // code...
header("location:login.php");
  die("Please login to continue");
}
 ?>
 <?php
include('intro.php');
  ?>
<div class="product_container">
<table>
  <tr>

    <th>Product Name</th>
    <th>Product Prices</th>
    <th>Product Carton</th>
    <th>Pieces in Carton</th>
    <th>Action</th>
  </tr>


  <?php
      $sql="SELECT * FROM products ";
      $result=$conn->query($sql);
      if ($result->num_rows>0) {
        while ($row=$result->fetch_assoc()) {
          echo "
          <tr>
              <td>".$row['product_name']."</td>
                <td>".$row['product_price']."</td>
                  <td>".$row['product_carton']."</td>
                    <td>".$row['pieces']."</td>
                    <td><a class='button_pro' href='delete.php?id=".$row['id']."'>DELETE</a> |
                    <a class='button_pro' href='edit.php?id=".$row['id']."'>Edit</a></td>
          </tr>";
        }
      }
   ?>
   <tr>
     <td></td>
     <td></td>
     <td></td>
     <td>Export As</td>
     <td><a href="Pd.php">PDF</a> || <a href="GenrateExcel.php"> Excel sheet</a></td>
   </tr>
</table>
</div>
