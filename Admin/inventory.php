<?php
include('Header.php');
if (empty($_SESSION['id'])) {
  // code...
header("location:login.php");
  die("Please login to continue");
}
 ?>
<div class="inventory_container">
  <form action="inventory.php" method="post">
    <table>
      <h2>Add product form</h2>
    <tr>
      <td><label for="product_name">Product Name</label> </td>
      <td><input class="input-field" type="text" required placeholder="Product Name" name="product_name"></td>
    </tr>
    <tr>
      <td><label for="product_price">Product Price</label></td>
      <td><input class="input-field" type="number" required placeholder="Product Price" name="product_price" min="100"></td>
    </tr>
    <tr>
      <td><label for="product_carton">Number of Carton</label></td>
      <td><input class="input-field" type="number" required placeholder="Number of Carton" name="product_quantity" min="1"></td>
    </tr>
    <tr>
      <td><label for="carton_pieces">Pieces Carton</label></td>
      <td><input class="input-field" type="number" required placeholder="Pieces Carton" name="carton_pieces" min="12"></td>
    </tr>

    <tr>
      <td><input class="input-field" type="reset"></td>
      <td><input class="input-field" type="submit" value="Add"></td>

    </tr>
  </table>
  </form>
</div>
<?php


if (!empty($_POST['product_name'])
    && !empty($_POST['product_price'])
    &&!empty($_POST['product_quantity'])
    &&!empty($_POST['carton_pieces'])) {
  // code...
        $product_name=$_POST['product_name'];
        $product_price=$_POST['product_price'];
        $product_quantity=$_POST['product_quantity'];
        $carton_pieces=$_POST['carton_pieces'];
        $iquery= "INSERT INTO products (product_name, product_price, product_carton,pieces)
        VALUES ('$product_name', '$product_price', '$product_quantity' , '$carton_pieces')";


        if ($conn->query($iquery)===TRUE) {
        // code...
        ?>
        <script>
        alert("Product is Added");
        </script>
        <?php
        }else {

        echo "error ".$iquery ."<br>" .$conn->error;
        }

}


 ?>


<div class="product_container">
<table>
  <tr>
    <th>Id</th>
    <th>Product Name</th>
    <th>Product Prices</th>
    <th>Product Carton</th>
    <th>Pieces in Carton</th>
  </tr>

  <?php
      $sql="SELECT * FROM products ";
        $result=$conn->query($sql);
      if ($result->num_rows>0) {
        while ($row=$result->fetch_assoc()) {
          echo "
          <tr>
            <td>".$row['id']."</td>
              <td>".$row['product_name']."</td>
                <td>".$row['product_price']."</td>
                  <td>".$row['product_carton']."</td>
                    <td>".$row['pieces']."</td>
          </tr>";
        }

      }
$conn->close();
   ?>
</table>
</div>
