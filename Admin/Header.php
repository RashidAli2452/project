<?php
session_start();
include('DB.php');
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Orelega+One&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="AdminCss/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>NCf</title>
  </head>
  <body>
  <div class="Nav_bar">
    <span style="cursor:pointer" onclick="openNav()"><img class="logo" src="images/logo.jpg" alt="logo"></span>
    <?php if (empty($_SESSION['id'])){?>
   <a href="signup.php"><i class="fa fa-fw fa-user" style="font-size:30px;color:black"></i></a>
    <?php } else { ?>
<a href="logout.php"><i class="fa fa-sign-out" style="font-size:30px;color:black" ></i></a>
      <a href="Dashboard.php"><i class="fa fa-fw fa-user" style="font-size:30px;color:black"></i><?php echo $_SESSION['name']; ?></a>
    <?php } ?>
  </div>
  <div id="myNav" class="overlay">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
    <a href="Dashboard.php">Dashboard</a>
  <a href="products.php">Products</a>
  <a href="Orders.php">Order Page</a>
  <a href="inventory.php">Add inventory</a>
  </div>
  </div>
  <script>
  function openNav() {
  document.getElementById("myNav").style.width = "40%";
  }
  function closeNav() {
  document.getElementById("myNav").style.width = "0%";
  }

  </script>
