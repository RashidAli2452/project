<?php
include('DB.php');
$sql="SELECT * FROM products ";
$result=$conn->query($sql);
$out="
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
</style>
<h2>New Choice Products List</h2>
<table>
<tr>
<th>Name</th>
<th>Carton Price</th>
<th>Total Cartons</th>
<th>Carton pieces</th>
</tr>
";
if ($result->num_rows>0) {
  while ($row=$result->fetch_assoc()) {
    $out.="
    <tr>
        <td>".$row['product_name']."</td>
          <td>".$row['product_price']."</td>
            <td>".$row['product_carton']."</td>
              <td>".$row['pieces']."</td>
    </tr>";
  }
}
$out.="</table>";
header( "Content-Type: application/vnd.ms-excel" );
header( "Content-disposition: attachment; filename=spreadsheet.xls" );
echo $out;

 ?>
