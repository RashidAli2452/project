
<?php
//include('Header.php');
include('DB.php');
$sql="SELECT * FROM products ";
$result=$conn->query($sql);
$out="
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
</style>
<table>
<tr>
<th>New choice Food products <th>
</tr>
<tr>
<td>Name</td>
<td>Carton Price</td>
<td>Total Cartons</td>
<td>Carton pieces</td>
</tr>
";
if ($result->num_rows>0) {
  while ($row=$result->fetch_assoc()) {
    $out.="
    <tr>
        <td>".$row['product_name']."</td>
          <td>".$row['product_price']."</td>
            <td>".$row['product_carton']."</td>
              <td>".$row['pieces']."</td>
    </tr>";
  }
}
$out.="</table>";
require_once 'dompdf/autoload.inc.php';
require 'vendor/autoload.php';
use Dompdf\Dompdf;
$doc = new Dompdf();
$doc->loadHtml($out);
$doc->setPaper('A4', 'portrait');
$doc->render();
$doc->stream();
 ?>
