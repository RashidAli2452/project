<?php
//include('Header.php');
include('DB.php');
$sql="SELECT * FROM `invoice`";
$result=$conn->query($sql);
$out="
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
</style>
<h1>New choice Food Order List</h1>
<table>
<tr>
<th>Bill Number</th>
<th>Date</th>
<th>Customer Name</th>
<th>Address</th>
<th>Total Price</th>
</tr>
";
if ($result->num_rows>0) {
  while ($row=$result->fetch_assoc()) {
    $out.="
    <tr>
    <td>".$row['Bill_no']."</td>
      <td>".$row['oreder_date']."</td>
        <td>".$row['customer_name']."</td>
          <td>".$row['address']."</td>
          <td>".$row['totalprice']."</td>
    </tr>";
  }
}
$out.="</table>";
require_once 'dompdf/autoload.inc.php';
require 'vendor/autoload.php';
use Dompdf\Dompdf;
$doc = new Dompdf();
$doc->loadHtml($out);
$doc->setPaper('A4', 'portrait');
$doc->render();
$doc->stream();
 ?>
