<?php

    include('Header.php');
    ?>
<div class="section group">
	<div class="col span_1_of_3">
    <div id="Image_shadow">
    <img class="Image_size" src="images/product_19.jpg">
    <div class="Product_info">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
<p class="product_data">Orange Stick </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data">  RS:210 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
    </div>
    </div>
	</div>
	<div class="col span_1_of_3">
	<div id="Image_shadow">
<img class="Image_size" src="images/product_20.jpg">
<div class="Product_info">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span><br>
<p class="product_data">Panda Stick </p><br>
<p class="product_data">|17 pieces in carton|</p>
<p class="product_data">  RS:400 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
</div>
  </div>
	</div>
	<div class="col span_1_of_3">
    <div id="Image_shadow">
  <img class="Image_size" src="images/product_21.jpg">
  <div class="Product_info">
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span><br>
<p class="product_data">KULFI </p><br>
<p class="product_data">|40 pieces in carton|</p>
<p class="product_data">  RS:330 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
  </div>
    </div>
	</div>
</div>
<div class="section group">
	<div class="col span_1_of_3">
    <div id="Image_shadow">
    <img class="Image_size" src="images/product_22.jpg">
    <div class="Product_info">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
<p class="product_data">Shahi Kulfi Stick </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data">  RS:420 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
    </div>
    </div>
	</div>
</div>

<div class="pagination">
  <a href="page2.php">&laquo;</a>
  <a href="HOME.php" >1</a>
  <a href="page2.php" >2</a>
  <a href="page3.php" class="active">3</a>

  <a href="#">&raquo;</a>
</div>
<?php
include('footer.php')
 ?>
