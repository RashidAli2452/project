-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2021 at 12:27 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newchoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `status`) VALUES
(4, 'Ali hamza', '2094@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 0),
(5, 'Rashid Ali', 'aalibhai651@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(100) NOT NULL,
  `Bill_no` varchar(200) NOT NULL,
  `oreder_date` date NOT NULL,
  `customer_name` varchar(200) NOT NULL,
  `address` varchar(300) NOT NULL,
  `totalprice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `Bill_no`, `oreder_date`, `customer_name`, `address`, `totalprice`) VALUES
(10, '1624379716', '2022-06-21', 'Ali hamza', 'shallay Valley', 54100),
(11, '1624384157', '2021-06-22', 'Ali hamza', 'shallay Valley', 2700),
(12, '1624535059', '2021-06-24', 'Ali hamza', 'lahore', 27600),
(13, '1624561165', '2021-06-24', 'Ali hamza', 'shallay Valley', 2947),
(14, '1624605727', '2021-06-25', 'Ali hamza', 'shallay Valley', 4597);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_items`
--

INSERT INTO `invoice_items` (`id`, `name`, `price`, `quantity`) VALUES
(56, 'choc dip', 900, 3),
(57, 'choc dip', 900, 1),
(58, 'shahi kulfee', 3000, 8),
(59, 'Shahi Kulfi Stick', 421, 7),
(60, 'Shahi Kulfi Stick', 421, 7),
(61, '2 in 1 Stick', 330, 5);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` double NOT NULL,
  `product_carton` int(11) NOT NULL,
  `pieces` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_price`, `product_carton`, `pieces`) VALUES
(19, 'PUNJABI SPECIAL', 600, 32, 40),
(20, 'CHOC DIP', 250, 10, 60),
(21, 'Big Slice Kulfa', 250, 34, 30),
(22, 'Nutty Stick', 450, 12, 30),
(23, 'FEAST Stick', 500, 23, 20),
(24, 'CHOCO BAR', 420, 12, 50),
(25, 'DONUT', 400, 23, 17),
(26, 'RED ANAAR Stick', 210, 23, 50),
(27, 'Strawbery Stick', 250, 23, 60),
(28, '2 in 1 Stick', 330, 12, 40),
(29, 'Peach Ice LOLLY', 420, 23, 50),
(30, 'COLA BLAST', 210, 12, 50),
(31, 'Sunday Cup', 400, 23, 24),
(32, 'Green Apple', 210, 12, 50),
(33, 'Khopra Stick', 250, 11, 60),
(34, 'Lychee Stick', 210, 33, 50),
(35, 'Mango Stick', 210, 22, 50),
(36, 'Milky Stick', 210, 22, 50),
(37, 'Orange Stick', 210, 22, 50),
(38, 'Panda Stick', 400, 22, 17),
(39, 'KULFI', 330, 22, 40),
(40, 'Shahi Kulfi Stick', 421, 22, 50);

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `u_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp`
--

INSERT INTO `temp` (`id`, `product_id`, `quantity`, `u_id`) VALUES
(53, 17, 5, 0),
(54, 18, 14, 0),
(55, 1, 1, 0),
(56, 17, 4, 17),
(57, 17, 3, 33),
(58, 17, 1, 33),
(59, 17, 3, 34),
(60, 18, 4, 34),
(61, 17, 7, 36),
(62, 18, 8, 33),
(63, 18, 1, 33),
(64, 40, 7, 34),
(65, 28, 5, 34),
(66, 36, 55, 33),
(67, 29, 1, 33),
(68, 23, 3, 37);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `email`, `password`) VALUES
(33, 'Ali hamza', '2094@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(34, 'Ali hamza', 'aalibhai651@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(35, 'Ali hamza', 'faheemamin533@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(36, 'Rashid Ali', 'Alihamzasbr@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(37, 'Zeshan haider', '2091@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `iamge` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `u_id`, `iamge`) VALUES
(12, 33, 'IMG-60d584d0cfd2d7.48739478.jpg'),
(13, 36, 'IMG-60d23503459e29.92955368.jpg'),
(29, 34, 'IMG-60d5a0378cf749.05215314.jpg'),
(30, 37, 'IMG-60d5a077691650.18790674.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp`
--
ALTER TABLE `temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `temp`
--
ALTER TABLE `temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
