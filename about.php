<?php

    include('Header.php');
    ?>

<div class="about-section">
  <h1> About New Choice Food</h1>

  <p>It's an ice cream factory located in Burhan Th:Hassan Abdal
	Distt: Attock</p>
  <p>Named by :: New Choice Food Products Burhan
	location:  Near police Chowki Burhan</p>
</div>

<h2 style="text-align:center">Our Team</h2>
<div class="row">
  <div class="column">
    <div class="card">
      <img src="images/Rashid.jpg" alt="Jane" style="width:100%">
      <div class="container">
        <h2>Rashid Ali</h2>
        <p class="title">CEO & Founder</p>
        <p>Owner of site and Manager of New .</p>
        <p>RAshidali@gmail.com.com</p>
          <p> <a href="ContactUs.php"  class="button">contact</a></p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="images/usama.jpg" alt="Rashid" style="width:100%">
      <div class="container">
        <h2>Usama Awan</h2>
        <p class="title">Group Member</p>
        <p>Web Engineering Project Group Member SAP 2564.</p>
        <p>awanzadasam@gmail.com</p>
        <p> <a href="ContactUs.php"  class="button">contact</a></p>

      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="images/Ali.jpg" alt="Ali" style="width:100%">
      <div class="container">
        <h2>Ali hamza</h2>
        <p class="title">Designer & Backend worker</p>
        <p>Web Engineering Project Group Member SAP 2094.</p>
        <p>Alihamzasbr@gmail.com</p>
          <p> <a href="ContactUs.php"  class="button">contact</a></p>
      </div>
    </div>
  </div>
</div>

</body>

<?php

    include('footer.php');
    ?>
    <style>
    body {

      margin: 0;
    }

    html {
      box-sizing: border-box;
    }

    *, *:before, *:after {
      box-sizing: inherit;
    }

    .column {
      float: left;
      width: 33.3%;
      margin-bottom: 16px;
      padding: 0 8px;
    }

    .card {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
      margin: 8px;
    }

    .about-section {
      padding: 50px;
      text-align: center;
      background-color: #474e5d;
      color: white;
    }

    .container {
      padding: 0 16px;
    }

    .container::after, .row::after {
      content: "";
      clear: both;
      display: table;
    }

    .title {
      color: grey;
    }

    .button {
      border: none;
      outline: 0;
      display: inline-block;
      padding: 8px;
      color: white;
      background-color: #000;
      text-align: center;
      cursor: pointer;
      width: 100%;
    }

    .button:hover {
      background-color: #555;
    }

    @media screen and (max-width: 650px) {
      .column {
        width: 100%;
        display: block;
      }
    }
    </style>
