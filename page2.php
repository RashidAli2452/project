<?php

    include('Header.php');
    ?>
<div class="section group">
	<div class="col span_1_of_3">
    <div id="Image_shadow">
    <img class="Image_size" src="images/product_10.jpg">
    <div class="Product_info">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
<p class="product_data">2 in 1 Stick </p><br>
<p class="product_data">|40 pieces in carton|</p>
<p class="product_data"> | RS:330 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
    </div>
    </div>
	</div>
	<div class="col span_1_of_3">
	<div id="Image_shadow">
<img class="Image_size" src="images/product_11.jpg">
<div class="Product_info">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span><br>
<p class="product_data">Peach Ice LOLLY </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data">  RS:420 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
</div>
  </div>
	</div>
	<div class="col span_1_of_3">
    <div id="Image_shadow">
  <img class="Image_size" src="images/product_12.jpg">
  <div class="Product_info">
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span><br>
<p class="product_data">COLA BLAST </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data"> RS:210 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
  </div>
    </div>
	</div>
</div>
<div class="section group">
	<div class="col span_1_of_3">
    <div id="Image_shadow">
    <img class="Image_size" src="images/product_26.jpg">
    <div class="Product_info">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
<p class="product_data">Sunday Cup </p><br>
<p class="product_data">|24 pieces in carton|</p>
<p class="product_data">  RS:400 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
    </div>
    </div>

	</div>
	<div class="col span_1_of_3">
	<div id="Image_shadow">
<img class="Image_size" src="images/product_14.jpg">
<div class="Product_info">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span><br>
<p class="product_data">Green Apple </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data">  RS:210|</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
</div>
  </div>
	</div>
	<div class="col span_1_of_3">
    <div id="Image_shadow">
  <img class="Image_size" src="images/product_15.jpg">
  <div class="Product_info">
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span><br>
<p class="product_data">Khopra Stick </p><br>
<p class="product_data">|60 pieces in carton|</p>
<p class="product_data">  RS:250 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
  </div>
    </div>
	</div>
</div>
<div class="section group">
	<div class="col span_1_of_3">
    <div id="Image_shadow">
    <img class="Image_size" src="images/product_16.jpg">
    <div class="Product_info">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span><br>
<p class="product_data">Lychee Stick </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data"> RS:210 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
    </div>
    </div>
	</div>
	<div class="col span_1_of_3">
	<div id="Image_shadow">
<img class="Image_size" src="images/product_17.jpg">
<div class="Product_info">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span><br>
<p class="product_data">Mango Stick </p><br>
<p class="product_data">|50 pieces in carton|</p>
<p class="product_data">  RS:210 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
</div>
  </div>
	</div>
	<div class="col span_1_of_3">
    <div id="Image_shadow">
  <img class="Image_size" src="images/product_18.jpg">
  <div class="Product_info">
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span><br>
<p class="product_data">Milky Stick </p><br>
<p class="product_data">|60 pieces in carton|</p>
<p class="product_data">  RS:200 |</p><br>
<a href="Cart.php"><p><button class="button-1">Add to Cart</button></p></a>
  </div>
    </div>
	</div>
</div>
<div class="pagination">
  <a href="HOME.php">&laquo;</a>
  <a href="HOME.php" >1</a>
  <a href="page2.php" class="active">2</a>
  <a href="page3.php">3</a>

  <a href="page3.php">&raquo;</a>
</div>
<?php
include('footer.php')
 ?>
